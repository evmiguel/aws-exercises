#!/user/bin/env groovy
@Library("jenkins-exercises-shared-library")_

pipeline {
    agent any
    tools {
        nodejs "my-nodejs"
    }
    stages {
        stage("Increment Version") {
            when {
                expression {
                    return env.GIT_BRANCH == "main"
                }
            }
            steps {
                script {
                    dir("app") {
                        incrementVersion()
                    }
                }
            }
        }
        stage("Run Tests") {
            steps {
                script {
                    dir("app") {
                        runTests()
                    }
                }
            }
        }
        stage("Build and push docker image") {
            when {
                expression {
                    return env.GIT_BRANCH == "main"
                }
            }
            steps {
                script {
                    buildImage "evmiguel/demo-app:${IMAGE_NAME}"
                    dockerLogin()
                    dockerPush "evmiguel/demo-app:${IMAGE_NAME}"
                }
            }
        }
        stage("Deploy application") {
            when {
                expression {
                    return env.GIT_BRANCH == "main"
                }
            }
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        def loginCmd =  "echo '${PASS}' | docker login -u '${USER}' --password-stdin"
                        def shellCmd = "bash ./server-cmds.sh evmiguel/demo-app:${IMAGE_NAME}"
                        def ec2Instance = "ec2-user@44.193.11.238"

                        sshagent(['bootcamp-key']) {
                            sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                            sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                            sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} \"${loginCmd} && ${shellCmd}\""
                        }
                    }
                }
            }
        }
        stage('Commit version update'){
            when {
                expression {
                    return env.GIT_BRANCH == "main"
                }
            }
            steps {
                script {
                    commitNewVersion("gitlab.com/evmiguel/aws-exercises.git", "main")
                }
            }
        }
    }
}